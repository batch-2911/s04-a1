@extends('layouts.app')

@section('content')
    <div class="text-center">
        <img src="https://cdn.freebiesupply.com/logos/large/2x/laravel-1-logo-png-transparent.png" alt="laravel" class="w-50" />

        <h1>Featured Post</h1>

        @if(count($posts) > 0)
            @foreach ($posts as $post) 

            <div class="card w-50 m-auto">
                <div class="card-body">
                    <h2><a href="/post/{{ $post->id }}">{{ $post->title }}</a></h2>

                    <p>Author: {{ $post->user->name }}</p>
                </div>
            </div>      
            @endforeach
        
        @else
            <div class="card w-50 m-auto">
                <div class="card-body">
                    <h2>No Posts Found</h2>
                </div>
            </div>
        @endif
    </div>
@endsection